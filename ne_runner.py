#!/usr/bin/python
#coding: utf-8
#import _mysql

import sys, getopt
import MySQLdb as mdb
import NLTKRunner as nltk
import util as util
import time
from datetime import date, timedelta

from sshtunnel import SSHTunnelForwarder


DB_NAME = 'eng_news_2015_09_30'

# global connection
con = None
cur = None
server=None
start=time.time()
# durations
nerTime = 0
curationTime = 0
identifyingNEsTime = 0
writeTime = 0

failure_sids = []


def connect(user, password, db='eng_news_2015_09_30', url='aspra16.informatik.uni-leipzig.de'):
    global server
    global con
    global cur    
    #forward ssh tunnel    
    server=SSHTunnelForwarder(
                            (url, 22),
                            ssh_password=password,
                            ssh_username=user,
                            remote_bind_address=(url, 3306))
    #start tunnel
    server.start()
    #connect to remote MySQL
    con = mdb.connect(user=user,
                          passwd=password,
                          db=db,
                          host='127.0.0.1',
                          port=server.local_bind_port)
    cur = con.cursor()

def connectLocal():
    global con
    global cur  
    con = mdb.connect(user="root", passwd="71421", db="eng_news", host='127.0.0.1')
    cur = con.cursor()
#
# Creates tables
#
def createNETables(jar,tagger):
    cur = con.cursor()
    TABLES = {}
    TABLES['ne_pos_based'] = ("CREATE TABLE IF NOT EXISTS `ne_pos_based` ("
                              "  `s_id` int(11) NOT NULL,"
                              " `start` int(11) NOT NULL,"
                              "  `end` int(11) NOT NULL,"
                              "  `ne` varchar(20) DEFAULT NULL,"
                              "  `tool_id` int(11) NOT NULL,"
                              "  PRIMARY KEY (`s_id`,`start`,`end`,`tool_id`)"
                              ")")
    TABLES['ne_tools'] = ("CREATE TABLE IF NOT EXISTS `ne_tools` ("
                        "  `tool_id` int(11) NOT NULL AUTO_INCREMENT,"
                        "  `name` varchar(100) DEFAULT NULL,"
                        "  `classifier` varchar(100) DEFAULT NULL,"
                        "  PRIMARY KEY (`tool_id`)"
                        ")")
    TABLES['ner_performed'] = ("CREATE TABLE IF NOT EXISTS `ner_performed` ("
                        "  `s_id` int(11) NOT NULL,"
                        "  `tool_id` int(11) NOT NULL,"
                        "  PRIMARY KEY (`s_id`,`tool_id`)"
                        ")")
    for tbl_name, ddl in TABLES.iteritems():
        try:
            print("Creating table {}: ".format(tbl_name))
            cur.execute(ddl)
            con.commit
        except mdb.Error as err:
            print(err)
        else:
            print("OK")
    
    # insert_tool
    tool_id = None;
    try:
        select = "SELECT tool_id FROM ne_tools WHERE name LIKE '{}' and classifier LIKE '{}' ORDER BY tool_id DESC;".format(jar,tagger)
        cur = con.cursor()
        cur.execute(select)
        result = cur.fetchall()    
        if (len(result)>0):
            tool_id = result[0][0]
            print 'Classifier already registered  under id: {}'.format(tool_id)
        else:    
            insert_statement = "INSERT INTO ne_tools(name,classifier) VALUES ('{}','{}')".format(jar,tagger)
            #print insert_statement
            cur.execute(insert_statement)    
            tool_id = cur.lastrowid
            con.commit()
            print 'Registered tool {} with classifier {} under id {}'.format(jar,tagger,tool_id)
    except mdb.Error, e: 
        print "Error inserting tool" 
        print "Error %d: %s" % (e.args[0], e.args[1])
    return tool_id

#
# Method used to retrieve a chunk of sentences of the given size
#
def getSentenceChunk(size):
    select = 'SELECT distinct s_id FROM inv_so NATURAL JOIN sources WHERE s_id NOT IN (SELECT s_id FROM ner_performed) LIMIT %s;'
    try:
        cur = con.cursor() 
        if(size<=0):
            cur.execute('SELECT distinct s_id FROM inv_so NATURAL JOIN sources WHERE s_id NOT IN (SELECT s_id FROM ner_performed);')
        else:
            cur.execute(select,int(size))        
        result = cur.fetchall()
        sentences = []
        i = 0
        for r in result:
            i=i+1
            sentences.append(r[0])
        print('Retrieved {} sentences for limit {}'.format(i,size))
    except mdb.Error, e:
        print "Error %d: %s" % (e.args[0], e.args[1])
    return sentences

################
# Insert NEs position based into MYSQL table
# expects ners to be a list of entries (NE, pos_list) whereas the first element is NE is the name of the NE
# and the second item "pos_list" a list of all sorted positions
#
def insertNERsPositionBased(s_id, ners, tool_id = 0, test=False):
    if len(ners)<=0:
        return 0
    upd_ner = []
    for entry in ners:
        if len(entry) == 2:
            pos_list = entry[1] # extract positions list
            if(len(pos_list)>0):
                pos_start = pos_list[0]
                pos_end = pos_list[-1]
                a = int(s_id), int(pos_start), int(pos_end), str(entry[0]), int(tool_id)
                upd_ner.append(a)
            else:
                print "empty pos_list: ", pos_list
        else:
            print "false entry in ner_list: ", entry
    #
    try:    
        cur = con.cursor()        
        insert_statement = "INSERT INTO ne_pos_based(s_id,start,end,ne,tool_id) VALUES {}".format(','.join(map(str,upd_ner))+";")
        #print insert_statement
        if(test==False):
            a = cur.execute(insert_statement)
            con.commit()
        #print('Successfully added {} NEs for sentence {} and tool {}'.format(a,s_id,tool_id))
    except mdb.Error, e:  
        print "Error %d: %s" % (e.args[0], e.args[1])

    return 1

# Marks a single sentence as already performed 
# Inserts s_id into DB table
#
def markSentenceAsNERPerformed(s_id, tool_id=0):
    cur = con.cursor()        
    sql = "INSERT INTO ner_performed(s_id, tool_id) VALUES ('%d', '%d')" % (s_id, tool_id)
    cur.execute(sql)
    con.commit()
    return
#
# Marks multiple sentences as already annotated with given tool
def markSentencesAsNERPerformed(sids, tool_id=0):
    updates = []
    for sid in sids:
        a = int(sid),int(tool_id)
        updates.append(a)
    cur = con.cursor()        
    insert_statement = "INSERT INTO ner_performed(s_id, tool_id) VALUES {}".format(','.join(map(str,updates))+";")
    #print insert_statement
    cur.execute(insert_statement)
    con.commit()
    return

def getWordsAndPositions(sids, tool_id):
    s_list = [] # plain words
    results = [] # words and positions      
    for s_id in sids:
        select = 'SELECT DISTINCT word, w_id, pos FROM inv_w NATURAL JOIN sentences NATURAL JOIN words WHERE s_id = %s ORDER BY pos' 
        try:
            cur = con.cursor() 
            cur.execute(select,s_id)
            
            result = cur.fetchall()
            token_list = [] 
            word_pos_list =[]
            # We artificially clean the results and only consider those positions with exactly ONE word
            # as WDT also recognizes sur- and forename pairs as one and thus don't need to give them twice
            for r in result:
                if len(r[0].split(" "))==1:
                    #print "split 1:", r
                    try:
                        insert = r[0].encode("utf8").replace("'","").replace('"', '').encode("utf8")
                        if(len(insert)>0):
                            token_list.append(insert)
                            word_pos_list.append((insert,r[2]))
                    except(UnicodeDecodeError):
                        dummy_s=''
                        for i in range(0, len(r[0])):
                            try: # only consider utf-8
                                dummy_s+=r[0][i].encode("utf8")
                            except(UnicodeDecodeError):
                                # should probably handle these cases separately?
                                a=0
                        # avoid quotes, they may break the sentences apart
                        insert = dummy_s.replace("'","").replace('"', '')
                        if(len(insert)>0):                          
                            token_list.append(insert)
                            word_pos_list.append((insert,r[2]))
            s_list.append(token_list)
            results.append(word_pos_list)
        except mdb.Error, e:
            print('Error Performing NER on sentence {}'.format(s_id))
            print "Error %d: %s" % (e.args[0], e.args[1])
    return (s_list,results)

# Main method handles annotating a bulk of sentences given by their s_id
#
#
def tagMultiple(sids, tool_id, jar, classifier, test=False, chunks_size = 10):
    global failure_sids
    global start
    global nerTime
    global curationTime
    global identifyingNEsTime
    global writeTime
    start = time.time() 
    words = getWordsAndPositions(sids, tool_id)
    s_list = words[0] # plain words
    results = words[1]# words and positions
    middle = time.time()
    print("Getting words for {} sentences took {} s".format(len(s_list), middle-start))
    ners = []
    # setting classifier
    nltk.setNERClassifier(jar, classifier)
    
    if(chunks_size<=0): # process all at once
        ners = resortAndProcess(sids, s_list, results, tool_id, jar, classifier, test=False)
    else: # process chunks of size chunks_size
        id_chunks = util.chunks(sids, chunks_size)
        s_list_chunks = util.chunks(s_list, chunks_size)
        results_chunks = util.chunks(results, chunks_size)
        if len(id_chunks) != len(s_list_chunks) or len(id_chunks) != len(results_chunks):
            print 'Chunking failed! Try to process at once...'
            ners = resortAndProcess(sids, s_list, results, tool_id, jar, classifier, test=False)
        else:
            for i in range(0, len(id_chunks)):
                print "Processing chunk {}  of length {} of all {} sentences...".format(i, len(id_chunks[i]), len(sids))
                ners + resortAndProcess(id_chunks[i], s_list_chunks[i],results_chunks[i], tool_id, jar, classifier, test=False)
                
    
    print "Overall identified {} NEs".format(len(ners))
    print " NER time = {} s, curationTime = {} s, identifyingPositionsTime = {} s, WriteMYSQLTime = {} s".format(nerTime,curationTime,identifyingNEsTime,writeTime)
    print " Overall {} s".format(time.time()-start)
    
    if len(failure_sids)>0 :
        print "Error on {} chunks...".format((len(failure_sids)))
        for f in failure_sids:
            print f
    
    return ners       

def resortAndProcess(sids, s_list, results, tool_id, jar, classifier, test=False):
    global nerTime
    global curationTime
    global identifyingNEsTime
    global writeTime
    global failure_sids
    try:
        
        middle =time.time()
        # run NER
        o_ners = nltk.runNERonSentences(s_list)
        end = time.time()   
        print("Performing NER on it took {} s".format( end-middle))
        nerTime+=end-middle
        middle = time.time()
        
        sum_ners = 0
        something_wrong = False
        ners = o_ners
        if(len(ners) != len(s_list)): # restore original order, if sentence borders were broken
            ners = util.rearrange(s_list, o_ners)
        end = time.time()
        print("Resorting NERs to results took {} s".format( end-middle))
        curationTime += end-middle
        middle =time.time()
    
        
        if s_list>0 and ners>0 and results>0 and len(s_list) == len(ners):
            #for each word i in sentences
                for i in range(0,len(s_list)):
                    my = time.time()
                    prev_tag = "none"
                    chunked=[]
                    positions = []
                    cur_sid = sids[i] # current sentence ID s_id
                    cur_res = results[i] # MYSQL result: word,w_id,pos
                    cur_ner = ners[i] # NER result: word, NER
                    
                    if(test): # debugging
                        print "cur_sid", cur_sid
                        print "cur_res",cur_res
                        print "cur", cur_ner
                        
                    if(len(cur_res)>len(cur_ner)):
                        print 'Original result longer'
                        
                    
                    for j in range(0,min(len(cur_ner),len(cur_res))):
                        pos = cur_res[j]
                        if(test==True):
                            print "handling position pos=",pos," j = ",j
                        nner = cur_ner[j]                    
                        if nner[1] in nltk.NERobjects: # if we have a NE
                            sum_ners = sum_ners+1
                            if nner[1] == prev_tag: 
                                positions[1].append(cur_res[j][1]) # append position
                            else:
                                if len(positions)>0:
                                    chunked.append(positions)
                                positions = [nner[1],[cur_res[j][1]]] # memorize (NE,pos)
                        else:
                            if len(positions)>0:
                                chunked.append(positions)
                            positions = []        
                        prev_tag = nner[1] # previous NE for next iteration
                    identifyingNEsTime+=time.time()-my
                    my = time.time()
                    insertNERsPositionBased(cur_sid, chunked, tool_id,test)
                    writeTime+=time.time()-my
        else: # debugging
            something_wrong = True
            print "len(s_list)", len(s_list), " len(ners)", len(ners) 
            print "len(results)", len(results), " len(ners)", len(ners) 
        end = time.time()
        print("Resorting and writing results to MYSQL took {} s".format( end-middle))
        middle = time.time()
        if(test==False and something_wrong == False): # no debug no error save results
            markSentencesAsNERPerformed(sids, tool_id)
        else:
            print 'Error on sentences ',sids
        end = time.time()
        print("Marked {} sentences as annotated took {} s".format( len(sids), end-middle))
        print("Overall {} s".format( end-start))        
        print("Found {} entities".format(sum_ners))
      
        if(something_wrong): # for debug
            for ner in s_list:
                print ner
            for ner in ners:
                print ner
            print("Error: Was not able to restore original order.")#should not happen
            failure_sids.append(sids)
        else:
            print("Everything went smoothly.")
        return ners
    except:
        print "Unexpected Failure on sids {}".format(sids)
        failure_sids.append(sids)


def main(argv):
    yesterday = date.today() - timedelta(2)
    #print yesterday.strftime('%m%d%Y')
    year = yesterday.strftime('%Y')
    month = yesterday.strftime('%m')
    day = yesterday.strftime('%d')
    print year+"_"+month+"_"+day

    user = 'aliss15b'
    password = None
    db='eng_news_'+year+'_'+month+'_'+day
    url='aspra16.informatik.uni-leipzig.de'
    base = '/usr/share/stanford/'
    jar=base+'stanford-ner.jar'
    tagger=base+'/classifiers/english.all.3class.distsim.crf.ser.gz'
    
    limit = -1
    chunks_size = 1000
    
    # used for option all
    eng_tagger=[base+'classifiers/english.muc.7class.distsim.crf.ser.gz',
                base+'classifiers/english.conll.4class.distsim.crf.ser.gz',
                base+'classifiers/english.all.3class.distsim.crf.ser.gz']
    s_id = -1
    allTaggers = False
    
    try:
        opts, args = getopt.getopt(argv,"u:p:r:d:j:t:s:l:k:a", ["user=", "password=", "url=", "db", "jar", "tagger", "sid", "limit", "chunksize", "all"])
    except getopt.GetoptError:
        print 'ne_runner.py -u <user> -p <password> -r <remote url> -d<database> -j<jar> -t<tagger> -l<limit> -k<size> -a<all>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'ne_runner.py -u <user> -p <password> -r <remote url> -d<database> -j<jar> -t<tagger> -l<limit> -k<size> -a<all>'
            sys.exit()
        elif opt in ("-u", "--user"):
            user = arg
        elif opt in ("-p", "--password"):
            password = arg
        elif opt in ("-r", "--url"):
            url = arg
        elif opt in ("-d", "--db"):
            db = arg
        elif opt in ("-j", "--jar"):
            jar = arg
        elif opt in ("-t", "--tagger"):
            tagger = arg    
        elif opt in ("-s", "--sid"):
            s_id = int(arg)
        elif opt in ("-a", "-all"):
            allTaggers = True
        elif opt in ("-l", "--limit"):
            limit =  int(arg)
        elif opt in ("-k", "--size"):
            chunks_size =  int(arg)
        
    print 'user is ', user
    print 'password is ', password
    print 'url is ', url
    print 'db is ', db
    print 's_id is', s_id
    print 'all is ', allTaggers

   
    if(password==None):
        print 'ne_runner.py -u <user> -p <password> -r <remote url> -d<database>'
        print 'password required!'
    
    
    if (s_id > 0):
        print "Perform util on s_id=",s_id
     
    connect(user, password, db, url)
   
    if(s_id > 0):
        if(allTaggers):
            for tag in eng_tagger:
                tagMultiple([s_id,12372], -1, jar, tag, True)
        else:
            tagMultiple([s_id,12372], -1, jar, tagger, True)
    else:
        start = time.time()
        tool_id = createNETables(jar,tagger)
        s_ids = getSentenceChunk(limit)
        if(allTaggers):
            for tag in eng_tagger:
                tool_id = createNETables(jar,tag)
                middle = time.time()    
                tagMultiple(s_ids, tool_id, jar, tag, False, chunks_size)
                end = time.time()
        else:
            tool_id = createNETables(jar,tagger)
            middle = time.time()    
            tagMultiple(s_ids, tool_id, jar, tagger, False, chunks_size)
            end = time.time()
        
        
        print("Overall NER on {} sentences took {} s".format(len(s_ids), end-start))
        print("   getting idperformNERonSentences in {}, NER and saving in {} s".format(middle-start, end-middle))
    
    
    #stop tunneled server
    server.stop()

if __name__ == "__main__":
   main(sys.argv[1:])
        