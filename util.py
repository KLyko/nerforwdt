import sys
import json
from __builtin__ import str


a_string = ["I've been dreaming\x96", ' He said:" sddsd "']

#
#
#
#
def rearrange(org_chunks, chunks):
#    if(len(org_chunks)==len(chunks)):
#        return chunks
    new_chunks = []
    i = 0 # pointer for current original chunks
    j = 0 # pointer for current chunks
    while(i<len(org_chunks)):
        #print i, j, len(org_chunks[i]),len(chunks[j])
        #print org_chunks[i]
        #print chunks[j]
        temp = chunks[j]
        if(len(org_chunks[i]) > len(temp)):
            while(len(org_chunks[i]) > len(temp)):
                j = j+1
                #print "-->j,len(tmp)",j,len(temp)
                #print "-->tmp", temp
                if len(chunks)>j:
                    temp=temp+chunks[j]
                else:
                    break
                #print "-->tmp",temp
                #print "-->len(tmp)",len(temp)
        #print "len(org_chunks[i]) vs.  len(temp)",len(org_chunks[i]), len(temp)
        if(len(org_chunks[i]) == len(temp)):
            #print "adding ==",temp
            new_chunks.append(temp)
            j=j+1
        else: 
            #print "adding >",temp[0:len(org_chunks[i])]
            new_chunks.append(temp[0:len(org_chunks[i])])
            last = len(chunks[j])
            chunks[j]=temp[len(org_chunks[i]):last]
            #print "setting chunk[j] to", temp[len(org_chunks[i]):last]
        i=i+1
    return new_chunks

def string_zeug():
    for s  in a_string:
        print s.encode('string-escape').replace('"', '\\"')   
        try:
            print s.encode('unicode-escape').replace(b'"', b'\\"')
        except(UnicodeDecodeError):
            print len(s)
            dummy_s=''
            for i in range(0, len(s)):
                try:
                    dummy_s+=s[i].encode("utf8")
                    #print s[i].encode("utf8")
                except(UnicodeDecodeError):
                    print ''
            print 'dummy: ',dummy_s
        print s.replace("'","").replace('"', '')



# chunks list n into lists of n elements
def chunks(l, n):
    result = []
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        result.append( l[i:i+n] )
    return result

list1=[
['But', '.'] ,
['"', 'find', 'out', '.'] ,
['a','b'] 
]
list2=[
[(u'But', u'O'), (u'.', u'O'), (u'"', u'O')] ,
[(u'find', u'O'), (u'out', u'O'), (u'.', u'O')] ,
[(u'a', u'O'), (u'b', u'O')] 
]


list5=[
['I', 'got', 'a', 'parking', 'ticket.'],
['I', 'grew', 'up', 'watching', 'all', 'the', 'UW', 'games', 'and', 'idolized', 'Coach', '(', 'Will', ')', 'Conroy', ',', 'Nate', 'Robinson', ',', 'Isaiah', '(', 'Thomas', ')', 'and', 'Brandon', 'Roy', 'so', 'just', 'thinking', 'that', 'I', 'could', 'actually', 'play', 'for', 'UW', 'it', 'was', 'like', 'how', 'can', 'I', 'turn', 'that', 'down', '.'],
['I', 'had', 'a', 'hard', 'time', 'not', 'playing', 'baseball', 'because', 'I', 'loved', 'baseball', 'so', 'much', '.'],
['I', 'havent', 'told', 'more', 'than', 'five', 'people', '.'],
]
list6=[[(u'I', u'O'), (u'got', u'O'), (u'a', u'O'), (u'parking', u'O'), (u'ticket.', u'O'), (u'I', u'O'), (u'grew', u'O'), (u'up', u'O'), (u'watching', u'O'), (u'all', u'O'), (u'the', u'O'), (u'UW', u'ORGANIZATION'), (u'games', u'O'), (u'and', u'O'), (u'idolized', u'O'), (u'Coach', u'O'), (u'(', u'O'), (u'Will', u'O'), (u')', u'O'), (u'Conroy', u'O'), (u',', u'O'), (u'Nate', u'PERSON'), (u'Robinson', u'PERSON'), (u',', u'O'), (u'Isaiah', u'PERSON'), (u'(', u'PERSON'), (u'Thomas', u'PERSON'), (u')', u'PERSON'), (u'and', u'O'), (u'Brandon', u'PERSON'), (u'Roy', u'PERSON'), (u'so', u'O'), (u'just', u'O'), (u'thinking', u'O'), (u'that', u'O'), (u'I', u'O'), (u'could', u'O'), (u'actually', u'O'), (u'play', u'O'), (u'for', u'O'), (u'UW', u'ORGANIZATION'), (u'it', u'O'), (u'was', u'O'), (u'like', u'O'), (u'how', u'O'), (u'can', u'O'), (u'I', u'O'), (u'turn', u'O'), (u'that', u'O'), (u'down', u'O'), (u'.', u'O')],
[(u'I', u'O'), (u'had', u'O'), (u'a', u'O'), (u'hard', u'O'), (u'time', u'O'), (u'not', u'O'), (u'playing', u'O'), (u'baseball', u'O'), (u'because', u'O'), (u'I', u'O'), (u'loved', u'O'), (u'baseball', u'O'), (u'so', u'O'), (u'much', u'O'), (u'.', u'O')],
[(u'I', u'O'), (u'havent', u'O'), (u'told', u'O'), (u'more', u'O'), (u'than', u'O'), (u'five', u'O'), (u'people', u'O'), (u'.', u'O')],
]


list_x1 = [1,2,3,4,5,6,7,8,9,10,11]
list_x2 = [1,2,3,4,5,6,7,8,9,10,11]
list_x3 = [1,2,3,4,5,6,7,8,9,10,11]

def main(args):

    
    new = rearrange(list5, list6)
    for l1 in range(0,len(list5)):
        print l1, list5[l1]
    for l1 in range(0,len(new)):
        print l1, new[l1]
        
if __name__ == "__main__":
   main(sys.argv[1:])
   