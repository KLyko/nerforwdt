#!/usr/bin/python
#coding: utf-8

# Resources to perform NER on WdT
# Uses NLTKs interface for the Stanford NER libary
# to install please refer to http://www.nltk.org/install.html
# also requires an installed JVM, and the Stanford libary with taggers and classifiers to be installed
#
import nltk
from nltk.tag.stanford import NERTagger

NERobjects = ['PERSON', 'ORGANIZATION', 'LOCATION','I-PER','I-ORG','I-LOC']


st = NERTagger('/usr/share/stanford/edu/stanford/nlp/models/ner/german.hgc_175m_600.crf.ser.gz','/usr/share/stanford/stanford-ner.jar')
# Used to perform NER on a sentence.

def setNERClassifier(jar,classifier):
    global st
    print('Setting classifier {} on tool {}'.format(classifier,jar))
    st = NERTagger(classifier,jar)

def runNER(sentence):
    tokens=nltk.word_tokenize(sentence)
    print 'tokens:'
    print tokens
    return runNERonTOKENS(tokens)

def to_unicode_or_bust(
    obj, encoding='utf-8'):
    if isinstance(obj, basestring):
        if not isinstance(obj, unicode):
            obj = unicode(obj, encoding)
    return obj

#####
# Performs NER on given tokens and returns a list of tuples with identified NEs.
# Uses NLTKs interface for the Stanford NER library
# to install please refer to http://www.nltk.org/install.html
# also requires an installed JVM, and the Stanford libary with taggers and classifiers to be installed
#
def runNERonTOKENS(tokens):
     
        ner_list=st.tag(tokens)
        #if len(ner_list) != len(tokens):
        harm_ner_list = []
        for token in tokens:
            for ner in ner_list[0]:
                # no handling of encoding issues here!
                if token==ner[0]:
                    harm_ner_list.append(ner)
                    break
        #print ('harmonized NER list: ', harm_ner_list)

        return harm_ner_list
    
# Rechunk is used to rechunk the same consequent entities. 
# E.g. Whether to have [('Klaus', 'Person), ('Lyko', 'Person'] we want only [('Klaus Lyko', 'Person'].
# Be aware that this method simply aligns consequent chunks, whether the refer to the same entity or not
def rechunk(ner_output):
    prev_tag = []
    chunked, pos = [], ""
    for word_pos in enumerate(ner_output):
        pos = word_pos
        if pos in ['PERSON', 'ORGANIZATION', 'LOCATION'] and pos == prev_tag:
            chunked[-1]+=word_pos
        else:
            chunked.append(word_pos)
        prev_tag = pos


    clean_chunked = [tuple([" ".join(wordpos[::2]), wordpos[-1]]) 
                    if len(wordpos)!=2 else wordpos for wordpos in chunked]

    return clean_chunked


#run()
#to ne_runner harmonizing ner_output
#print runNERonTOKENS(['Boulders', 'lie in', 'lie', 'in', 'in front', 'in front of', 'front', 'of', 'a', 'fireplace', 'on', 'on the', 'the', 'second-floor', 'landing', 'as though', 'as', 'though', 'they', ',', 'like', 'an', 'evil', 'Santa Claus', 'Santa', 'Claus', ',', 'hurtled', 'down', 'the', 'chimney', '.'])

def runNERonSentences(sentences):
    ners_list=st.tag_sents(sentences)
    return ners_list

###
# Examples
def run():
    list1 =[['Boulders', 'lie in', 'lie', 'in', 'in front', 'in front of', 'front', 'of', 'a', 'fireplace', 'on', 'on the', 'the', 'second-floor', 'landing', 'as though', 'as', 'though', 'they', ',', 'like', 'an', 'evil', 'Santa', 'Claus', ',', 'hurtled', 'down', 'the', 'chimney', '.'],
           ['I', 'grew', 'up', 'watching', 'all', 'the', 'UW', 'games', 'and', 'idolized', 'Coach', '(', 'Will', ')', 'Conroy', ',', 'Nate', 'Robinson', ',', 'Isaiah', '(', 'Thomas', ')', 'and', 'Brandon', 'Roy', 'so', '.'],
          ]
    
    list2a=[['Die','Firma','Eichhorn','GmbH','hat','im','Januar','2015','Insolvenz','angemeldet','.'],
           ['Die', 'CDU','Bundesregierung','hat','das','Zuwanderungsgesetz', 'beschlossen','.'],
           ['Klaus', 'fährt', 'Skier', 'von', 'der', 'Firma', 'Foobar','in','der','Schweiz' ],]
    
    list2b= [
           ['Klaus', 'fährt', 'Skier', 'von', 'der', 'Firma', 'Foobar','in','der','Schweiz' ],
           ['Die','Firma','Eichhorn','GmbH','hat','im','Januar','2015','Insolvenz','angemeldet','.'],
           ['Die', 'CDU','Bundesregierung','hat','das','Zuwanderungsgesetz', 'beschlossen','.'],]
    
    result = runNERonSentences(list2a+list2b)
    for res in result:
        for word in res:
            if word[1] in NERobjects:
                print word

if __name__ == "__main__":
    run()