### Project NER for WDT ###

* A student project for the University of Leipzig
* Aims to utilize Named Entity Recognition (NER) for the "Wörter des Tages" (WdT)
* still under heavy development and prone to bugs

### Set up ###
Requirements

* Python 2,7++

* JVM to run the Stanford NER framework

* Stanford NER tool and classifiers, download unpack. E.g. *to */usr/share/stanford/*

Python libraries:

* NLTK  http://www.nltk.org/install.html 

* sshtunnel (run *pip install sshtunnel*) in order to connect to the WDT MySQL instances

### Run ###
In order to run our tool you have to set up your local environment as stated above. Furthermore, you'll need an SSH access to the WdT-MySQL server under the same credentials as to its MySQL instance. 

The main file is the ne_runner.py file. It requires a list of parameters, either run 

```
#!python

python ne_runner.py -h
```

or inspect its main() method.

At least you'll need to specify your SSH/MySQl credentials:

```
#!python
python ne_runner.py -u <username> -p <password>
```
This minimal specification will try to annotate the eng_news database from two days ago using the standard configuration of Stanford with the default classifier at: */usr/share/stanford/classifiers/english.all.3class.distsim.crf.ser.gz.tsim.crf.ser.gz*.

other parameters are

* *-d/--db* : specify the name of the WdT database to use

* *-j/--jar* : specify location of the StanfordNER.jar (default: */usr/share/stanford/*)

* *-t/--tagger* : specify location of the Stanford classifier to (default */usr/share/classifiers/english.all.3class.distsim.crf.ser.gz.tsim.crf.ser.gz*)


* *-l/--limit* : Limit of sentences to retrieve (default: no limit)

* *-k/--size* : Size of sentence chunks to perform NER simultaneous on (default: 1000)

To setup other classifiers, especially other languages, follow the instructions at [http://nlp.stanford.edu/software/CRF-NER.shtml](http://nlp.stanford.edu/software/CRF-NER.shtml): download and unpack any new models to your Stanford folder, adjust your classpath, and specify to use them via the *-t* parameter.

### Contact / Team ###
Klaus Lyko (lyko@informatik.uni-leipzig.de)